import PropTypes from "prop-types";
import injectSheet from "react-jss";

const styles = theme => ({
  header: {
    backgroundColor: "#888",
    color: theme.main.colors.header,
    padding: `1.5rem 1.5rem  calc(${theme.bars.sizes.actionsBar}px + 1.5rem) 1.5rem`,
    "& p": {
      margin: 0
    },
    [`@media (min-width: ${theme.mediaQueryTresholds.M}px)`]: {
      padding: `2rem 2.5rem  calc(${theme.bars.sizes.actionsBar}px + 2rem) 2.5rem`
    },
    [`@media (min-width: ${theme.mediaQueryTresholds.L}px)`]: {
      padding: `2rem 3rem 2rem 3rem`
    }
  }
});

const Header = props => {
  const { classes, txt} = props;
  const { html } = txt;

  return (
    <header className={classes.header}>
      <h3 content={html} />
    </header>
  );
};

Footer.propTypes = {
  classes: PropTypes.object.isRequired,
  txt: PropTypes.string.isRequired
};

export default injectSheet(styles)(Header);