---
title: footnote
---

* a tech blog by [luke zautke](https://lukezautke.com)
*  | &nbsp;&nbsp;adapted from a theme by the esteemed [greg lobinski](https://www.greglobinski.com)
*  | &nbsp;&nbsp;with [Gatsby](https://www.gatsbyjs.org/) & [React](https://reactjs.org)
*  | &nbsp;&nbsp;source code on [GitHub](https://github.com/zautke/2bit_freestyle)
*  | &nbsp;&nbsp;hosted on [Amazon Web Services](https://aws.amazon.com)
*  | &nbsp;&nbsp;HTTP/2 negotiated by [Nginx](https://www.nginx.com)

