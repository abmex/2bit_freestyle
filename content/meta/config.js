const colors = require("../../src/styles/colors");

module.exports = {
  siteTitle: "2-Bit Freestyle - a blog by luke", // <title>
  shortSiteTitle: "2-Bit Freestyle", // <title> ending for posts and pages
  //siteDescription: "Capricious musings, memory-offloads, and other tasty tidbits.",
  siteDescription: "coder blog",
  siteUrl: "https://blog.lukezautke.com",
  pathPrefix: "",
  siteImage: "preview.jpg",
  siteLanguage: "en",
  // author
  authorName: "luke zautke",
  authorTwitterAccount: "abstract_luke",
  // info
  infoTitle: "luke zautke",
  //infoTitleNote: "Capricious musings, memory-offloads, and other tasty tidbits.",
  infoTitleNote: "coder blog",
  // manifest.json
  manifestName: "2-Bit Freestyle - a blog by luke",
  manifestShortName: "01Freestyle", // max 12 characters
  manifestStartUrl: "/",
  manifestBackgroundColor: colors.bg,
  manifestThemeColor: colors.bg,
  manifestDisplay: "standalone",
  // social
  authorSocialLinks: [
    { name: "github", url: "https://github.com/zautke" },
    { name: "twitter", url: "https://twitter.com/abstract_luke" },
    { name: "facebook", url: "http://facebook.com/lukezautke" }
  ]
};
