---
title: About this blog
menuTitle: About this blog
---

Hello, My name is **Luke** &#10088;&#9996;&#10089;. Welcome to my blog!

This web journal is a reboot of a previous effort that went stale several years ago &mdash; as, unfortunately, things in life at times find the agency to crowd out others. I have been behind recently in contributing back to the community and this effort is one way to recycle hard-won knowledge, learn through teaching, and even bore/annoy some with random thoughts and musings.

I would ask your patience, as this project is shiny new and, at best, in _**early**_ beta. I love the theme's look-and-feel (the nuanced skewing of the thumbnails' border radii and the subtle hover-over animation sold me), but I'm looking to expand the design and functionality in several directions over the coming days and weeks.

I am also pledging to stop using my Twitter feed as a half-duplex consumption pipeline. Follow me, <a class="twitter-follow-button" data-show-count=false
href="https://twitter.com/abstract_luke">
@abstract_luke</a>, and keep informed on the progress and travails of this project. And, oh &mdash; content will be appearing rapidly, as well.

In closing, it deserves mention that this [GatsbyJS](https://www.gatsbyjs) thing is pretty freaking cool. I encourage the "React-ionaries"[^1] out there <small>( <... crowd boos ...> )</small> to give it a look, there is a lot of good stuff tucked in there. I'm enjoying myself immensely already.

Alright. `</discourse>`

<br />

-----
<small>[^1] Sorry, Pythonistas &mdash; your monopoly is broken.</small>



